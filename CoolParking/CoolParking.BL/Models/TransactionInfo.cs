﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.

using System;
using System.Globalization;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        private readonly string _vehicleId;
        private readonly DateTime _transactionTime;
        public decimal Sum { get; set; }

        public TransactionInfo(string vehicleId, decimal sum)
        {
            _vehicleId = vehicleId;
            Sum = sum;
            _transactionTime = DateTime.Now;
        }

        public override string ToString()
        {
            return "-----------------------------\n" + 
                _transactionTime.ToString("F", CultureInfo.CreateSpecificCulture("en-US")) + 
                $"\nVehicle: {_vehicleId}\nSum: {Sum}";
        }
    }
}