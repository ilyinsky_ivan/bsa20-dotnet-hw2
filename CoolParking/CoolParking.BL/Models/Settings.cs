﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.

using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public class Settings
    {
        public static decimal ParkingBalance = 0;
        public static int ParkingCapacity = 10;
        public static double TransactionPeriod = 5;
        public static double LogWritingPeriod = 60;
        public static Dictionary<VehicleType, decimal> PriceRates;
        public static decimal FineCoefficient = 2.5m;

        static Settings()
        {
            PriceRates = new Dictionary<VehicleType, decimal>(4)
            {
                [VehicleType.PassengerCar] = 2,
                [VehicleType.Truck] = 5,
                [VehicleType.Bus] = 3.5m,
                [VehicleType.Motorcycle] = 1
            };

        }
    }
}