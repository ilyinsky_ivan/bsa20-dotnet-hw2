﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.

using System;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        private string _id;
        public VehicleType VehicleType { get; private set; }
        public decimal Balance { get; internal set; }

        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        { 
            this.Id = id;
            this.VehicleType = vehicleType;
            this.Balance = balance;
        }

        public string Id
        {
            get => _id;
            private set
            {
                if (ValidateId(value))
                    _id = value;
                else
                    throw new ArgumentException("ID doesn't correspond to pattern!");
            }
        }

        private static bool ValidateId(string id)
        {
            var regex = new Regex(@"[A-Z]{2}-\d{4}-[A-Z]{2}");
            return regex.IsMatch(id);
        }

        public static string GenerateRandomRegistrationPlateNumber()
        {
            return GenerateTwoRandomLetters() + '-' + GenerateRandomNumberValue() + '-' + GenerateTwoRandomLetters();
        }

        private static string GenerateTwoRandomLetters()
        {
            var rand = new Random();
            var letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray();
            var word = "";
            for (var i = 0; i < 2; i++)
            {
                var letterNum = rand.Next(0, letters.Length - 1);
                word += letters[letterNum];
            }

            return word;
        }

        private static string GenerateRandomNumberValue()
        {
            var min = 1000;
            var max = 9999;
            Random rand = new Random();

            return rand.Next(min, max).ToString();
        }

        public override string ToString()
        {
            return $"Vehicle ID: {Id}\nVehicle type: {VehicleType}\nVehicle balance: {Balance}";
        }
    }
}
