﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.

using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        private static Parking _instance;

        public decimal Balance { get; internal set; } = Settings.ParkingBalance;
        public int Capacity { get; internal set; } = Settings.ParkingCapacity;
        public List<Vehicle> Vehicles { get; } = new List<Vehicle>(Settings.ParkingCapacity);

        private Parking() { }

        public static Parking GetInstance()
        {
            return _instance ??= new Parking();
        }
    }
}