﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.

using System;
using System.IO;
using CoolParking.BL.Interfaces;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        private static string _logPath;

        public string LogPath
        {
            get => _logPath;
            set
            {
                if(!string.IsNullOrWhiteSpace(value))
                    _logPath = value;
                else
                    throw new ArgumentException("Path to log file is null or empty!");
            }
        }

        public LogService(string path)
        {
            LogPath = path;
        }

        public string Read()
        {
            try
            {
                using var file = new StreamReader(LogPath);
                return file.ReadToEnd();
            }
            catch (Exception e)
            {
                throw new InvalidOperationException($"Reading failed: {e}");
            }
        }

        public void Write(string logInfo)
        {
            if (string.IsNullOrWhiteSpace(logInfo)) return;
            try
            {
                using var file = new StreamWriter(LogPath, true);
                file.WriteLine(logInfo);
            }
            catch (Exception e)
            {
                throw new ApplicationException($"Writing failed: {e}");
            }
        }
    }
}