﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.

using CoolParking.BL.Interfaces;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        public event ElapsedEventHandler Elapsed;
        public double Interval { get; set; }
        private Timer _timer;

        public void Start()
        {
            var milliseconds = Interval * 1000;
            _timer = new Timer(milliseconds);
            _timer.Elapsed += Elapsed;
            _timer.AutoReset = true;
            _timer.Enabled = true;
        }
        public void Stop()
        {
            _timer.Stop();
        }
        public void Dispose()
        {
            _timer.Dispose();
        }
    }
}