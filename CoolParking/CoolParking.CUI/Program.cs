﻿using System;
using System.Threading;

namespace CoolParking.CUI
{
    internal class Program
    {
        protected static int OrigRow = Console.CursorTop;
        protected static int OrigCol = Console.CursorLeft;

        private static void Main()
        {
            PrintWrap();

            ApplyInput(PrintMainMenu());

            Console.ReadKey();
        }

        public static void ApplyInput(string choice)
        {
            switch (choice)
            {
                case "1":
                    {
                        InputHandler.GetParkingBalance();
                        Thread.Sleep(1000);
                        PrintWrap();
                        var input = PrintMainMenu();
                        ApplyInput(input);
                        break;
                    }
                case "2":
                    {
                        InputHandler.GetCurrentIncome();
                        Thread.Sleep(1000);
                        PrintWrap();
                        var input = PrintMainMenu();
                        ApplyInput(input);
                        break;
                    }
                case "3":
                    {
                        InputHandler.GetPlacesInfo();
                        Thread.Sleep(1000);
                        PrintWrap();
                        var input = PrintMainMenu();
                        ApplyInput(input);
                        break;
                    }
                case "4":
                    {
                        InputHandler.GetCurrentTransactions();
                        Thread.Sleep(5000);
                        PrintWrap();
                        var input = PrintMainMenu();
                        ApplyInput(input);
                        break;
                    }
                case "5":
                    {
                        InputHandler.GetTransactionsHistory();
                        Thread.Sleep(5000);
                        PrintWrap();
                        var input = PrintMainMenu();
                        ApplyInput(input);
                        break;
                    }
                case "6":
                    {
                        InputHandler.GetVehicles();
                        Thread.Sleep(5000);
                        PrintWrap();
                        var input = PrintMainMenu();
                        ApplyInput(input);
                        break;
                    }
                case "7":
                    {
                        InputHandler.AddVehicle();
                        Thread.Sleep(500);
                        PrintWrap();
                        var input = PrintMainMenu();
                        ApplyInput(input);
                        break;
                    }
                case "8":
                    {
                        InputHandler.RemoveVehicle();
                        Thread.Sleep(500);
                        PrintWrap();
                        var input = PrintMainMenu();
                        ApplyInput(input);
                        break;
                    }
                case "9":
                    {
                        InputHandler.TopUpBalance();
                        Thread.Sleep(500);
                        PrintWrap();
                        var input = PrintMainMenu();
                        ApplyInput(input);
                        break;
                    }
                case "e":
                    {
                        Environment.Exit(0);
                        break;
                    }
                default:
                    {
                        Console.WriteLine("Incorrect choice!");
                        break;
                    }

            }
        }

        private static void WriteAt(string s, int x, int y)
        {
            try
            {
                Console.SetCursorPosition(OrigCol + x, OrigRow + y);
                Console.Write(s);
            }
            catch (ArgumentOutOfRangeException e)
            {
                Console.Clear();
                Console.WriteLine(e.Message);
            }
        }

        private static string PrintMainMenu()
        {
            WriteAt("Main menu", 19, 3);

            WriteAt("1. Get parking balance\n"
                    + " 2. Get current incomes\n"
                    + " 3. Get amount of free/occupied spaces\n"
                    + " 4. Get all current transactions\n"
                    + " 5. Get history of transaction\n"
                    + " 6. Get list of vehicles at parking\n"
                    + " 7. Put vehicle\n"
                    + " 8. Take vehicle\n"
                    + " 9. Top up vehicle balance\n"
                    + " e. Exit\n", 1, 5);



            Console.SetCursorPosition(OrigCol + 1, OrigRow + 16);
            Console.Write("Your choice: ");
            return Console.ReadLine();
        }


        private static void PrintWrap()
        {
            Console.Clear();

            for (var i = 0; i < 50; i++)
            {
                WriteAt("-", i, 0);
            }

            WriteAt("Welcome to CoolParking service!", 10, 1);

            for (var i = 0; i < 50; i++)
            {
                WriteAt("-", i, 2);
            }

            for (var i = 0; i < 20; i++)
            {
                WriteAt("-", 0, i);
            }

            for (var i = 0; i < 20; i++)
            {
                WriteAt("-", 49, i);
            }

            for (var i = 0; i < 50; i++)
            {
                WriteAt("-", i, 20);
            }
        }
    }
}
