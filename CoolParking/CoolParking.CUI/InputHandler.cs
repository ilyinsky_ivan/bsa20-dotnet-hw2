﻿using System;
using CoolParking.BL.Services;
using System.IO;
using System.Linq;
using System.Reflection;
using CoolParking.BL.Models;

namespace CoolParking.CUI
{
    internal class InputHandler
    {
        private static readonly ParkingService ParkingService;
        static InputHandler()
        {
            var logger = new LogService($@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log");
            var logTimer = new TimerService();
            var withdrawTimer = new TimerService();
            ParkingService = new ParkingService(withdrawTimer, logTimer, logger);
        }

        public static void GetParkingBalance()
        {
            Console.Clear();
            Console.WriteLine("Parking balance is: " + ParkingService.GetBalance());
        }

        public static void GetCurrentIncome()
        {
            Console.Clear();
            var income = ParkingService.GetLastParkingTransactions()
                .Select(tr => tr.Sum).Sum();
            Console.WriteLine("Parking current income is: " + income);
        }

        public static void GetPlacesInfo()
        {
            Console.Clear();
            var free = ParkingService.GetFreePlaces();
            var capacity = ParkingService.GetCapacity();
            Console.WriteLine($"Free: {free}\nOccupied: {capacity - free}");
        }

        public static void GetCurrentTransactions()
        {
            Console.Clear();
            var transactions = ParkingService.GetLastParkingTransactions();
            if (transactions.Length == 0) 
                Console.WriteLine("No transactions yet...");
            else
            {
                var list = string.Join("\n", transactions.Select(tr => tr.ToString()));
                Console.WriteLine("Current transactions:\n" + list);
            }
        }

        public static void GetTransactionsHistory()
        {
            Console.Clear();
            var list = ParkingService.ReadFromLog();
            if (string.IsNullOrWhiteSpace(list))
                Console.WriteLine("No transactions at log :(");
            else
                Console.WriteLine("All transactions:\n" + list);
        }

        public static void GetVehicles()
        {
            Console.Clear();
            var vehicles = ParkingService.GetVehicles();
            if (!vehicles.Any())
                Console.WriteLine("No vehicles...");
            else
            {
                const string separator = "\n----------------------\n";
                var result = string.Join(separator, vehicles.Select(vehicle => vehicle.ToString()));
                Console.WriteLine("All vehicles:\n" + result);
            }
        }

        public static void AddVehicle()
        {
            Console.Clear();
            var id = Vehicle.GenerateRandomRegistrationPlateNumber();
            var type = GetVehicleType();
            var balance = TopUpVehicleWhenFirstTime();
            try
            {
                var vehicle = new Vehicle(id, type, balance);
                ParkingService.AddVehicle(vehicle);
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine("Failed adding vehicle:\n" + ex.Message);
            }
        }

        private static VehicleType GetVehicleType()
        {
            Console.Clear();
            Console.WriteLine("Choose type of vehicle:\n"
                              + "1. PassengerCar\n"
                              + "2. Truck\n"
                              + "3. Bus\n"
                              + "4. Motorcycle\n"
                              + "e. Cancel\n");
            Console.Write("Your choice: ");
            var input = Console.ReadLine();
            return input switch
            {
                "1" => VehicleType.PassengerCar,
                "2" => VehicleType.Truck,
                "3" => VehicleType.Bus,
                "4" => VehicleType.Motorcycle,
                _ => default
            };
        }

        private static decimal TopUpVehicleWhenFirstTime()
        {
            Console.Clear();
            Console.Write("Enter sum: ");
            var result = Convert.ToDecimal(Console.ReadLine());
            return result;
        }

        public static void RemoveVehicle()
        {
            Console.Clear();
            Console.Write("Enter vehicle ID: ");
            var id = Console.ReadLine();
            try
            {
                ParkingService.RemoveVehicle(id);
                Console.WriteLine("Successfully removed");
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine("Removing failed:\n" + ex.Message);
            }
        }

        public static void TopUpBalance()
        {
            Console.Clear();
            Console.Write("Enter vehicle ID: ");
            var id = Console.ReadLine();
            var sum = TopUpVehicleWhenFirstTime();
            try
            {
                ParkingService.TopUpVehicle(id, sum);
                Console.WriteLine($"Topped up vehicle with {id} on {sum}");
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine("Failed:\n" + ex.Message);
            }
        }
    }
}
